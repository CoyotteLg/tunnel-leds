# Tunnel LEDs

This project allows to control WS2812 LEDs using an arduino.
Its main purpose is to light a tunnel on my HOe scale train layout.

The program simulate out of order lights that can flash on an random intermittent base.