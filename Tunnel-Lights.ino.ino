/***************************************************************************
 * Tunnel-Lights.ino
 * Gère l'éclairage d'un tunnel avec des LEDs de type WS2812
 * 
 * Ce code simule une panne intermittente de l'une des LED.
 * En générale, une lampe en panne clignotte de manière intermittente mais régulière.
 * 
 * Afin de varier les plaisirs
 *   - La LED concernée, le nombre de flashs et leur durée est aléatoire
 *   - La LED en panne change au bout d'un temps aléatoire lui aussi.
 *   - Il peut ne pas y avoir de panne durant un temps aléatoire lui aussi.
 ***************************************************************************/
 
#include <FastLED.h>

#define LED_PIN                   7     // N° de Pin de la chaine de LED
#define NUM_LEDS                  5     // nbr de LED
#define MAX_FLASHES               12    // Nombre max détapes d'un flash
#define RND_OFFSET                2     // voir initFlash()
#define MIN_FLASH_DELAY           5     // durée minimale d'une étape de flash en ms
#define MAX_FLASH_DELAY           80    // durée maximale d'une étape de flash en ms
#define MIN_BETWEEN_FLASH_DELAY   800   // durée minimale entre deux flash en ms
#define MAX_BETWEEN_FLASH_DELAY   3000  // durée maximale entre deux flash en ms
#define MIN_REINIT_DELAY          10000 // durée minimale avant le paramétrage d'un nouveau flash en ms
#define MAX_REINIT_DELAY          15000 // durée maximale avant le paramétrage d'un nouveau flash en ms
#define R_VALUE                   50    // Valeur pour la led rouge
#define G_VALUE                   30    // Valeur pour la led verte
#define B_VALUE                   7     // Valeur pour la led bleue

CRGB leds[NUM_LEDS];                    // Instances des LEDs
int OoOLed;                             // N° de la LED hors service (Out of Order)
int FlashSteps;                         // Nombre d'étapes du flash actuel
int FlashDelays[MAX_FLASHES];           // Durées des étapes du flash actuel
int Step;                               // Etape
long nextReinit = 0;                    // heure en ms du prochain paramétrage d'un flash...

//-------------------------------------------------------------------------------
// Reinitialise l'état des LEDs...
void reinitLEDs( void ) {
  for (int i = 0; i < NUM_LEDS; i++ ) {
    leds[i] = CRGB(R_VALUE, G_VALUE, B_VALUE);
  }
  FastLED.show();    
}

//-------------------------------------------------------------------------------
// Change les paramètres du flash de panne...

void initFlash() {
  // On choisi le nombre de flash (0 est une option possible, mais 1 est exclus... )
  FlashSteps = 1;
  while( FlashSteps == 1 ) { 
    // RND OFFSET permet d'augmenter le nombre de chances de ne pas avoir de lampe en panne...
    FlashSteps = (int) max( 0, random( MAX_FLASHES + RND_OFFSET ) - RND_OFFSET ); 
  }

  // Si une lampe est en panne...
  if (FlashSteps > 0) {
    // on choisi celle-ci au hasard mais ce ne peut être la première ou la dernière
    // On veut que l'entrée et la sortie du tuennel soient toujours correctement éclairées...
    // C'est un choix arbitraire totalement assumé par l'auteur :-D
    OoOLed = (int)random(1, NUM_LEDS-1 );      
  
    // On défini la durée de chaque étape...
    for( int i = 0; i < FlashSteps - 1; i++ ) {
      FlashDelays[i] = (int) random( MIN_FLASH_DELAY, MAX_FLASH_DELAY ); 
    }
    // la dernière étape détermine le temps entre deux flashs...
    FlashDelays[FlashSteps - 1] = (int) random( MIN_BETWEEN_FLASH_DELAY, MAX_BETWEEN_FLASH_DELAY );     
  }
  // on détermine aussi le moment du prochain changement de flash...
  nextReinit += random( MIN_REINIT_DELAY, MAX_REINIT_DELAY ); 
}

//-------------------------------------------------------------------------------
void setup() {
  // on crée les instances de LED
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  // On réinitialise toutes les LEDs à leur état de départ
  reinitLEDs();
  // On initialise la fonction random... avec une valeur alléatoire...
  randomSeed(analogRead(0));
  // On initialise le premier flash...
  initFlash();
}

//-------------------------------------------------------------------------------
void loop() {
  Step = 0;  // On redémarre à la première étape...
  
  // Tant qu'il y a des étapes...
  while( Step < FlashSteps ) {
    // Si l'étape est paire, on étaient la led, sinon on la rallume
    leds[OoOLed] = (Step % 2) == 0 ? CRGB(0,0,0): CRGB(R_VALUE, G_VALUE, B_VALUE);
    // Mise à jour de la situation
    FastLED.show();
    // On patiente la durée nécessaire et on passe à l'étape suivante..
    delay(FlashDelays[Step++]);
  } 

  // Si le temps est venu de changer de type de flash...
  if ( millis() > nextReinit ) {
    reinitLEDs(); // On réinitialise toutes les LEDs
    initFlash();  // On calcule de nouveaux paramètres...
  }
}
